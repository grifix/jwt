<?php

declare(strict_types=1);

namespace Grifix\Jwt\Tests;

use Firebase\JWT\JWT;
use Grifix\Date\DateTime\DateTime;
use Grifix\Jwt\Exceptions\InvalidTokenSignatureException;
use Grifix\Jwt\Exceptions\InvalidTokenValueException;
use Grifix\Jwt\Exceptions\TokenExpiredException;
use Grifix\Jwt\Exceptions\TokenNotValidYetException;
use Grifix\Jwt\FirebaseJwt;
use Grifix\Jwt\Payload;
use PHPUnit\Framework\TestCase;

final class FirebaseJwtTest extends TestCase
{
    public function testItEncodes(): void
    {
        $jwt = new FirebaseJwt('example_key', 'HS256');
        $payload = Payload::create(['foo' => 'bar']);
        $token = $jwt->encode($payload);
        self::assertEquals($payload, $jwt->decode($token));
    }

    public function testItFailsOnExpiredToken(): void
    {
        $jwt = new FirebaseJwt('example_key', 'HS256');
        JWT::$timestamp = DateTime::create(2001, 1, 1, 12)->getTimestamp();
        $payload = Payload::create([], expiresAt: DateTime::create(2001, 1, 1, 11));
        $token = $jwt->encode($payload);
        $this->expectException(TokenExpiredException::class);
        $jwt->decode($token);
    }

    public function testItFailsIfTokenNotValidYet(): void
    {
        $jwt = new FirebaseJwt('example_key', 'HS256');
        JWT::$timestamp = DateTime::create(2001, 1, 1, 12)->getTimestamp();
        $payload = Payload::create([], notBefore: DateTime::create(2001, 1, 1, 13));
        $token = $jwt->encode($payload);
        $this->expectException(TokenNotValidYetException::class);
        $jwt->decode($token);
    }

    public function testItFailsOnInvalidSignature(): void
    {
        $jwt = new FirebaseJwt('example_key', 'HS256');
        $jwt2 = new FirebaseJwt('other_key', 'HS256');
        $token = $jwt->encode(Payload::create([]));
        $this->expectException(InvalidTokenSignatureException::class);
        $jwt2->decode($token);
    }

    public function testItFailsOnInvalidValue(): void
    {
        $this->expectException(InvalidTokenValueException::class);
        (new FirebaseJwt('test'))->decode('invalid_token');
    }
}
