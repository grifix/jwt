<?php

declare(strict_types=1);

namespace Grifix\Jwt\Tests;

use Grifix\Date\DateTime\DateTime;
use Grifix\Jwt\Exceptions\ValueDoestNotExistException;
use Grifix\Jwt\Payload;
use PHPUnit\Framework\TestCase;

final class PayloadTest extends TestCase
{
    public function testItWorks(): void
    {
        $issuer = 'https://issuer.example.com';
        $subject = 'user@example.com';
        $audience = 'https://audience.example.com';
        $issuedAt = DateTime::create(2001, 1, 1, 2, 15, 49);
        $expiresAt = DateTime::create(2001, 1, 1, 15, 22, 10);
        $notBefore = DateTime::create(2001, 1, 1, 18, 55, 13);
        $id = '03978821-40ed-40f7-9fc5-d59b105507c2';

        $data = [
            'user' =>
                [
                    'id' => 1,
                    'email' => 'user@example.com',
                ],
        ];

        $payload = Payload::create(
            $data,
            $issuer,
            $subject,
            $audience,
            $expiresAt,
            $notBefore,
            $issuedAt,
            $id,
        );

        self::assertEquals(
            [
                'jti' => $id,
                'iss' => $issuer,
                'sub' => $subject,
                'iat' => $issuedAt->getTimestamp(),
                'aud' => $audience,
                'exp' => $expiresAt->getTimestamp(),
                'user' => $data['user'],
                'nbf' => $notBefore->getTimestamp(),
            ],
            $payload->toArray(),
        );

        self::assertEquals($id, $payload->getId());
        self::assertEquals($issuer, $payload->getIssuer());
        self::assertEquals($subject, $payload->getSubject());
        self::assertEquals($issuedAt, $payload->getIssuedAt());
        self::assertEquals($audience, $payload->getAudience());
        self::assertEquals($data['user']['id'], $payload->getValue('user.id'));
        self::assertEquals($data['user']['email'], $payload->getValue('user.email'));
    }

    public function testItFailsToGetNotExistedValue(): void
    {
        $payload = Payload::create([]);
        $this->expectException(ValueDoestNotExistException::class);
        $this->expectExceptionMessage('Value with path [user.id] doest not exist in JWT token!');
        $payload->getValue('user.id');
    }
}
