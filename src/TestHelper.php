<?php

declare(strict_types=1);

namespace Grifix\Jwt;

use Firebase\JWT\JWT;
use Grifix\Date\DateTime\DateTime;

abstract class TestHelper
{
    public static function freezeCurrentDate(DateTime $dateTime): void
    {
        JWT::$timestamp = $dateTime->getTimestamp();
    }

    public static function unfreezeCurrentDate(): void
    {
        JWT::$timestamp = null;
    }
}
