<?php

declare(strict_types=1);

namespace Grifix\Jwt;

use Grifix\ArrayWrapper\ArrayWrapper;
use Grifix\Date\DateTime\DateTime;
use Grifix\Jwt\Exceptions\ValueDoestNotExistException;

final class Payload
{
    private ArrayWrapper $data;

    /**
     * @param mixed[] $data
     */
    private function __construct(
        array $data = [],
        private readonly ?string $issuer = null,
        private readonly ?string $subject = null,
        private readonly ?string $audience = null,
        private readonly ?DateTime $expiresAt = null,
        private readonly ?DateTime $notBefore = null,
        private readonly ?DateTime $issuedAt = null,
        private readonly ?string $id = null,
    ) {
        $this->data = ArrayWrapper::create($data);
    }

    /**
     * @param mixed[] $data
     */
    public static function create(
        array $data = [],
        ?string $issuer = null,
        ?string $subject = null,
        ?string $audience = null,
        ?DateTime $expiresAt = null,
        ?DateTime $notBefore = null,
        ?DateTime $issuedAt = null,
        ?string $id = null,
    ): self {
        return new self(
            $data,
            $issuer,
            $subject,
            $audience,
            $expiresAt,
            $notBefore,
            $issuedAt,
            $id,
        );
    }

    /**
     * @return mixed[]
     */
    public function toArray(): array
    {
        return array_merge(
            $this->data->getWrapped(),
            [
                'iss' => $this->issuer,
                'sub' => $this->subject,
                'aud' => $this->audience,
                'exp' => $this->expiresAt?->getTimestamp(),
                'nbf' => $this->notBefore?->getTimestamp(),
                'iat' => $this->issuedAt?->getTimestamp(),
                'jti' => $this->id,
            ],
        );
    }

    public function getValue(string $path): mixed
    {
        if (!$this->data->hasElement($path)) {
            throw new ValueDoestNotExistException($path);
        }
        return $this->data->getElement($path);
    }

    public function getIssuer(): ?string
    {
        return $this->issuer;
    }

    public function getSubject(): ?string
    {
        return $this->subject;
    }

    public function getAudience(): ?string
    {
        return $this->audience;
    }

    public function getIssuedAt(): ?DateTime
    {
        return $this->issuedAt;
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getExpiresAt(): ?DateTime
    {
        return $this->expiresAt;
    }

    public function getNotBefore(): ?DateTime
    {
        return $this->notBefore;
    }

    /**
     * @param mixed[] $data
     */
    public static function fromArray(array $data): self
    {
        $originalData = $data;
        unset(
            $data['iss'],
            $data['sub'],
            $data['aud'],
            $data['exp'],
            $data['nbf'],
            $data['iat'],
            $data['jti']
        );
        return new self(
            $data,
            self::getStringValue($originalData, 'iss'),
            self::getStringValue($originalData, 'sub'),
            self::getStringValue($originalData, 'aud'),
            self::getDateValue($originalData, 'exp'),
            self::getDateValue($originalData, 'nbf'),
            self::getDateValue($originalData, 'iat'),
            self::getStringValue($originalData, 'jti'),
        );
    }

    /**
     * @param mixed[] $data
     */
    private static function getStringValue(array $data, string $key): ?string
    {
        if (!array_key_exists($key, $data)) {
            return null;
        }
        /** @var string $result */
        $result = $data[$key];

        return $result;
    }

    /**
     * @param mixed[] $data
     */
    private static function getDateValue(array $data, string $key): ?DateTime
    {
        if (!isset($data[$key])) {
            return null;
        }

        /** @var int $value */
        $value = $data[$key];

        return DateTime::fromTimestamp($value);
    }
}
