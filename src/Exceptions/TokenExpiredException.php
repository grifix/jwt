<?php

declare(strict_types=1);

namespace Grifix\Jwt\Exceptions;

final class TokenExpiredException extends \Exception
{
    public function __construct(\Throwable $previous)
    {
        parent::__construct('Token expired!', previous: $previous);
    }
}
