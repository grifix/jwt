<?php

declare(strict_types=1);

namespace Grifix\Jwt\Exceptions;

final class TokenNotValidYetException extends \Exception
{
    public function __construct(\Throwable $previous)
    {
        parent::__construct('Token not valid yet!', previous: $previous);
    }
}
