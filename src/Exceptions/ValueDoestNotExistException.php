<?php

declare(strict_types=1);

namespace Grifix\Jwt\Exceptions;

final class ValueDoestNotExistException extends \Exception
{
    public function __construct(string $path)
    {
        parent::__construct(sprintf('Value with path [%s] doest not exist in JWT token!', $path));
    }
}
