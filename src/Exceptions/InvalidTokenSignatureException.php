<?php

declare(strict_types=1);

namespace Grifix\Jwt\Exceptions;

final class InvalidTokenSignatureException extends \Exception
{
    public function __construct(\Throwable $previous)
    {
        parent::__construct('Invalid token signature!', previous: $previous);
    }
}
