<?php

declare(strict_types=1);

namespace Grifix\Jwt;

use Firebase\JWT\BeforeValidException;
use Firebase\JWT\ExpiredException;
use Firebase\JWT\JWT;
use Firebase\JWT\Key;
use Firebase\JWT\SignatureInvalidException;
use Grifix\Jwt\Exceptions\InvalidTokenSignatureException;
use Grifix\Jwt\Exceptions\InvalidTokenValueException;
use Grifix\Jwt\Exceptions\TokenExpiredException;
use Grifix\Jwt\Exceptions\TokenNotValidYetException;

final class FirebaseJwt implements JwtInterface
{
    public function __construct(private readonly string $key, private readonly string $algorithm = 'HS256')
    {
    }

    public function encode(Payload $payload): string
    {
        return JWT::encode($payload->toArray(), $this->key, $this->algorithm);
    }

    /**
     * @inheritdoc
     */
    public function decode(string $token): Payload
    {
        try {
            $decoded = JWT::decode($token, new Key($this->key, $this->algorithm));
        } catch (BeforeValidException $exception) {
            throw new TokenNotValidYetException($exception);
        } catch (ExpiredException $exception) {
            throw new TokenExpiredException($exception);
        } catch (SignatureInvalidException $exception) {
            throw new InvalidTokenSignatureException($exception);
        } catch (\UnexpectedValueException $exception) {
            throw new InvalidTokenValueException($exception);
        }

        return Payload::fromArray((array)$decoded);
    }
}
