<?php

declare(strict_types=1);

namespace Grifix\Jwt;

use Grifix\Jwt\Exceptions\InvalidTokenSignatureException;
use Grifix\Jwt\Exceptions\InvalidTokenValueException;
use Grifix\Jwt\Exceptions\TokenExpiredException;
use Grifix\Jwt\Exceptions\TokenNotValidYetException;

interface JwtInterface
{
    public function encode(Payload $payload): string;

    /**
     * @throws InvalidTokenSignatureException
     * @throws TokenExpiredException
     * @throws TokenNotValidYetException
     * @throws InvalidTokenValueException
     */
    public function decode(string $token): Payload;
}
